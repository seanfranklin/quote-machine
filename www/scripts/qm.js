String.prototype.replaceAt = function(index, string) {
    return this.substr(0, index) + string + this.substr(index + 1, this.length);
};


const qm = {

    machine:       document.getElementById('machine'),
    modeSelect:    document.getElementById('mode-select'),
    input:         document.getElementsByTagName('input')[0],
    searchIcon:    document.getElementById('search-icon'),
    quoteDiv:      document.getElementById('quote'),
    topLeds:       document.getElementsByClassName('led-top'),
    bottomLeds:    document.getElementsByClassName('led-bottom'),

    pageFontSize:  16,
    quoteFontSize: 0,
    quoteArea:     0,
    mode:          'random',
    ownModes:      ['orwell', 'sealab'],
    random:        [],
    orwell:        [],
    sealab:        [],
    userTitles:    [],
    userTitle:     null,
    nextQuote:     null,
    checkInterval: null,
    ledInterval:   null,
    showInterval:  null,


    /**
     * Toggles color of top leds in succession from left to right.
     */
    blinkLeds() {
        qm.topLeds[0].classList.toggle('led-blue');
        setTimeout(() => {
            qm.topLeds[1].classList.toggle('led-blue');
        }, 200);
        setTimeout(() => {
            qm.topLeds[2].classList.toggle('led-blue');
        }, 400);
    },


    /**
     * There are currently three modes: random (default), orwell and sealab.
     */
    changeMode(mode = qm.mode) {
        document.body.classList.remove(qm.mode);
        document.body.classList.add(mode);
        qm.mode = mode;
    },


    /** 
     * Sometimes when the quote is short and the attribution is long, the attribution
     * pushes the quote div out to the right, which un-centers the text and looks awful.
     * Note that whiteWidth actually excludes the quote div’s left and right margins.
     */
    adjustAttribution() {
        const whiteWidth = qm.machine.clientWidth - (qm.pageFontSize * 1.6 * 2),
             attribution = qm.quoteDiv.getElementsByClassName('attribution')[0];

        if (attribution && whiteWidth < attribution.clientWidth) {
            attribution.style.fontSize = '0.6em';
            // console.info('Adjusted attribution font size.');
        }
    },


    /** 
     * Sometimes the font size cannot be guessed accurately and
     * must be adjusted after the quote has been added to the DOM.
     */
    adjustFontSize(quoteLength = qm.quoteDiv.innerText.length) {
        const machineHeight = qm.machine.clientHeight,
                whiteHeight = machineHeight - (qm.pageFontSize * 2 * 2),
                quoteHeight = qm.quoteDiv.clientHeight;

        //console.log('fontSize before adjust: ' + window.getComputedStyle(qm.quoteDiv).fontSize);
        let difference = whiteHeight - quoteHeight;
        if (difference < 20 || difference > 55) {
            if (difference < 20) {
                difference = difference - 20;
            }
            qm.quoteDiv.style.fontSize = Math.min(Math.max(1, qm.quoteFontSize * qm.quoteArea) + (difference / (600 - quoteLength)), 2.2) + 'em';
        }
        //console.log('fontSize after adjust: ' + window.getComputedStyle(qm.quoteDiv).fontSize);
    },


    /**
     * Guess and set the quote’s font size based on the length of the
     * quote and the area of the white space in the quote wrapper.
     */
    setFontSize(quoteLength = qm.quoteDiv.innerText.length) {
        qm.quoteFontSize = Math.max(1, 2.2 - (quoteLength / 10) / 10);
        qm.quoteDiv.style.fontSize = Math.min(Math.max(1, qm.quoteFontSize * qm.quoteArea), 2.2) + 'em';
    },


    /**
     * The whitespace available to display the quote depends on the window size
     * so its area must be calculated for use in determining the quote’s font size.
     */
    setQuoteArea() {
        const width = qm.machine.offsetWidth <= 600 ? qm.machine.offsetWidth : 600,
             height = qm.machine.offsetHeight <= 371 ? qm.machine.offsetHeight : 371,
            divisor = (width < 384 || width > 480) ? 170000 : 140000;
       qm.quoteArea = width * height / divisor;
    },


    /**
     * If user is online then get a random quote from the source that
     * corresponds to the current mode: either Wikiquote or own server.
     */
    getQuote(mode = qm.mode) {
        if (navigator.onLine) {
            if (mode === 'random') {
                qm.getWikiQuote();
            } else {
                qm.getOwnQuote(mode);
            }
        } else {
            qm.nextQuote = '<span class="error">Could not load quote because you are offline.</span>';
        }
    },


    /**
     * Gets a random quote for current mode from own server.
     */
    getOwnQuote(mode = qm.mode) {
        const request = new XMLHttpRequest();
        request.open('POST', '/.netlify/functions/quote', true);

        request.onreadystatechange = () => {
            if (request.readyState === 4) {
                let quote = request.responseText;
                if (request.status === 200 && typeof quote === 'string' && quote.length) {
                    if(~qm[mode].indexOf(quote)) {
                        // console.info('Quote recently displayed. Getting another...');
                        qm.getOwnQuote(mode);
                    } else {
                        if (quote.split(' ').length > 6) {
                            qm.nextQuote = quote.replaceAt(quote.lastIndexOf(' '), '&nbsp;');
                        } else {
                            qm.nextQuote = quote;
                        }
                    }
                } else {
                    qm.nextQuote = '<span class="error">Could not load quote.</span>';
                }
            }
        };
        request.send(mode);
    },


    /**
     * Gets a random title (e.g. person or film) from titles object.
     */
    getRandomTitle() {
        return titles[Math.floor(Math.random() * titles.length)];
    },


    /**
     * Strips tags and comments from the passed string, excluding any allowed tags.
     */
    stripTags(string, allowedTags) {
        const tags = /<\/?([a-z][a-z0-9]*)\b[^>]*>/ig,
            commentsAndPhpTags = /<!--[\s\S]*?-->|<\?(?:php)?[\s\S]*?\?>/ig;

        // Make sure the allowedTags arg is a string containing only lowercase tags
        allowedTags = (((allowedTags || '') + '')
            .toLowerCase()
            .match(/<[a-z][a-z0-9]*>/g) || [])
            .join('');
  
        return string
            .replace(commentsAndPhpTags, '')
            .replace(tags, function($0, $1) {
                return allowedTags.indexOf('<' + $1.toLowerCase() + '>') > -1 ? $0 : '';
            });
    },


    /**
     * Some quotes returned from Wikiquote API (wqa script) are messed up
     * so try to clean them up to make them display-worthy.
     */
    formatQuote(quote) {
        const punctuation = ['.', '!', '?', ',', ':', ';', '"', '\'', '…', '-', '–', '—', '”', '’', ')'];
        // quote = quote.replace(/<a[\S\s]*?>([\S\s]*?)<\/a>/ig, '$1');  // Remove <a> tags
        quote = qm.stripTags(quote, '<br><br/>');  // Strip all tags except <br>

        quote = quote
            .replace(/<br\/*>/g, ' ')       // Change break tags to spaces
            .replace(/\[\]|\(\)/g, '')      // Remove empty brackets
            .replace(/\s\s+/g, ' ')         // Condense multiple spaces
            .replace(/\s,/g, ',')           // Remove spaces before commas
            .replace(/\s\.(\s|$)/g, '.')    // Remove spaces before full stops
            .replace(/^\s+|\s+$/g, '');     // Remove leading and trailing spaces
        
        /* Remove leading spaces if any exist
        while (quote.charAt(0) === ' ') {
            quote = quote.substring(1, quote.length);
        } */
        
        /* Remove trailing spaces if any exist
        while (quote.charAt(quote.length - 1) === ' ') {
            quote = quote.substring(0, quote.length - 1);
        } */
        
        quote = quote.charAt(0).toUpperCase() + quote.substring(1, quote.length);
        const endPuncIndex = punctuation.indexOf(quote.charAt(quote.length - 1));

        // If there is no end punctuation then add full stop at end 
        if (endPuncIndex === -1) {
            quote += '.';

        // If end punctuation is colon or comma then replace with ellipses
        } else if (endPuncIndex === 3 || endPuncIndex === 4) {
            quote = quote.substring(0, quote.length - 1) + '...';
        }
        
        // Convert last space to be non-breaking so last word doesn’t wrap
        if (quote.split(' ').length > 6) {
            quote = quote.replaceAt(quote.lastIndexOf(' '), '&nbsp;');
        }

        // Since shape-inside isn’t available the quote can be shaped with floats
        quote = '<div class="spacer left"></div><div class="spacer right"></div>' + quote;
        return quote;
    },


    /**
     * Attempt to get a suitable random quote from Wikiquote.
     */
    getWikiQuote(title = qm.userTitle || qm.getRandomTitle()) {
        let attempts = 0;
        qm.nextTitle = title;

        (function attemptToGetQuotes() {
            function error(message = 'Failed to get a decent quote') {

                // Failed to get a suitable quote for user-entered title on the
                // first attempt, which usually means the user entered a bad title
                if (attempts === 0 && qm.userTitle) {
                    //console.log(`Error: ${message}. Try another mode.`);
                    qm.nextQuote = `<span class="error">${message}. Try another mode.</span>`;
                    qm.userTitles.splice(qm.userTitles.indexOf(qm.userTitle), 1);

                // Sometimes an entire section of a page doesn’t have any suitable
                // quotes, so we can try again and hopefully get a better section
                } else if (++attempts < 5) {
                    //console.log(`Error: ${message}. Trying again...`);
                    attemptToGetQuotes();

                // After more than 5 attempts it’s time to give up on this title
                } else {
                    //console.log(`Error: ${message}. Trying different title...`);
                    //qm.nextQuote = `<span class="error">${message}.</span>`;
                    qm.getQuote();
                }
            }

            // Get a section of quotes from a page on Wikiquotes
            if (typeof wqa === 'object') {
                wqa.getRandomQuotes(qm.nextTitle, (obj) => {
                    const quotes = obj.quotes,
                        //attribution = '—&nbsp;' + qm.nextTitle.replace(/ /g, '&nbsp;');
                        attribution = qm.nextTitle.replace(/ /g, '&nbsp;'),
                        attributionClass = attribution.length > 30 ? 'attribution long' : 'attribution';
                    
                    // Process a random quote from the array of quotes returned from Wikiquotes
                    (function processNextQuote() {
                        if (typeof quotes === 'object' && quotes.length) {
                            let quoteIndex = Math.floor(Math.random() * quotes.length),
                                quote = quotes[quoteIndex],
                                minQuoteLength = 110 + attribution.length;  // Includes attribution span and spacer divs
                            quote = (typeof quote === 'string' && quote.length) ? `${qm.formatQuote(quote)} <span class="${attributionClass}">${attribution}</span>` : null;
                        
                            // If quote is an unsuitable length or a repeat then remove it from
                            // quotes array before processing another random quote if any are left
                            if (!quote || quote.length < minQuoteLength || quote.length > 300 || ~qm.random.indexOf(quote)) {
                                quotes.splice(quoteIndex, 1);
                                if (quotes.length) {
                                    //console.log('Quote not suitable. Processing next quote...');
                                    processNextQuote();
                                } else {
                                    error('Failed to get a decent quote');
                                }
                            } else {
                                attempts = 5;
                                qm.nextQuote = quote;
                            }
                        } else {
                            error();
                        }
                    })();
                }, (errorMessage) => {
                    error(errorMessage);
                });
            } else {
                qm.nextQuote = console.log('<span class="error">Wikiquote API isn’t loaded.</span>');
            }
        })();
    },


    /**
     * Once a quote has been chosen it needs to be shown or this is all pointless.
     */
    showQuote(quote = qm.nextQuote) {
        let quoteLength = quote.length - 60;
        if (qm.mode === 'random' && !qm.userTitle) {
            quoteLength -= 50;
        }
        qm.setFontSize(quoteLength);
        qm.machine.classList.remove('disable', 'loading');
        clearInterval(qm.ledInterval);
        qm.quoteDiv.innerHTML = quote;
        qm.adjustFontSize(quoteLength);
        qm.mode === 'random' && qm.adjustAttribution();
        clearInterval(qm.showInterval);
        qm.nextQuote = null;

        // Show next quote after 1 minute
        qm.showInterval = setInterval(() => {
            document.hidden || qm.quoteDiv.classList.contains('hidden') || qm.changeQuote();
        }, 60000);

        qm[qm.mode].push(quote);
        if (qm[qm.mode].length > 19) {
            qm[qm.mode].shift();
        }
    },


    /**
     * Manages the process of swapping the current quote for another one.
     */
    changeQuote() {
        let numChecks = 0;
        qm.quoteDiv.innerHTML = '';

        if (typeof qm.nextQuote === 'string') {
            qm.showQuote();
            qm.getQuote();
        } else {
            qm.blinkLeds();
            qm.ledInterval = setInterval(() => {
                qm.blinkLeds();
            }, 600);
            qm.machine.classList.add('disable', 'loading');

            qm.checkInterval = setInterval(() => {
                if (typeof qm.nextQuote === 'string') {
                    clearInterval(qm.checkInterval);
                    qm.showQuote();
                    qm.getQuote();
                } else if (numChecks === 2000) {
                    console.log('Error: could not load quote.');
                    qm.nextQuote = '<span class="error">Could not load quote.</span>';
                }
                numChecks++;
            }, 10);
        }
    },


    /**
     * Sets the value of the input box to an example title.
     */
    setInputExample(exampleTitle = '') {
        while (exampleTitle < 3 || exampleTitle > 16) {
            exampleTitle = qm.getRandomTitle();
        }
        qm.input.placeholder = 'e.g. ' + exampleTitle;
    },


    /**
     * Hides the mode-select div and shows a quote.
     */
    hideModeSelect() {
        qm.modeSelect.classList.add('hidden');
        qm.quoteDiv.classList.remove('hidden');
        qm.machine.classList.remove('disabled');
        qm.machine.title = 'Click to load another quote';
    },


    /**
     * Shows the mode-select div, which allows the user to enter a title.
     * The title is used to search Wikiquote and hopefully find some quotes.
     */
    showModeSelect() {
        qm.machine.title = '';
        qm.machine.classList.remove('loading');
        qm.machine.classList.add('disabled');
        qm.setInputExample();
        qm.quoteDiv.classList.add('hidden');
        qm.modeSelect.classList.remove('hidden');
        qm.input.focus();
    },


    /**
     * Toggles display of the mode-select div.
     */
    toggleModeSelect() {
        if (qm.quoteDiv.classList.contains('hidden')) {
            qm.hideModeSelect();
        } else {
            qm.showModeSelect();
        }
    },


    /**
     * Deal with user input, which should be a title to get quotes for.
     * This function can be triggered in a variety of ways.
     */
    userInput(inputValue = qm.input.value) {
        inputValue = inputValue.replace(/<\/?[^>]+(>|$)/g, '') || '';

        let potentialMode = inputValue.toLowerCase(),
            mode = ~qm.ownModes.indexOf(potentialMode) ? potentialMode : 'random',
            pathname = inputValue ? inputValue.replace(/\s/g, '_') : '/';

        qm.input.value = inputValue;
        history.state === inputValue || history.pushState(inputValue, '', `/${pathname}/`);

        if (inputValue) {
            qm.searchIcon.classList.remove('hidden');
        } else {
            qm.searchIcon.classList.add('hidden');
        }
        qm.mode === mode || qm.changeMode(mode);
        qm.userTitle = (mode === 'random') ? inputValue : null;

        if (qm.userTitle && qm.userTitles.indexOf(qm.userTitle) === -1) {
            qm.userTitles.push(qm.userTitle);
        }
        clearInterval(qm.checkInterval);
        clearInterval(qm.showInterval);
        qm.nextQuote = null;
        qm.getQuote();
        qm.hideModeSelect();
        qm.changeQuote();
    },


    /**
     * This website uses features such as viewport units and flex-box,
     * which older browsers don’t support. Check machine dimensions 
     * and inform the user of any major display issues.
     */
    hasFeatures() {
        const style = window.getComputedStyle(qm.machine),
              width = parseInt(style.width),
             height = parseInt(style.height);

        if (style.display === 'inline-flex' &&
           (width === 600 || width > window.innerWidth / 2) &&
           (height === 371 || height > width / 2)) {
            return true;
        }
    }

};


for (let i = 0; i < qm.bottomLeds.length; i++) {
    qm.bottomLeds[i].onclick = (event) => {
        event.stopPropagation();
        event.target.classList.toggle('led-blue');
        qm.toggleModeSelect();
    };
}

qm.machine.onclick = (event) => {
    event.stopPropagation();
    ~qm.machine.className.indexOf('disabled') || qm.changeQuote();
};

qm.input.onclick = (event) => {
    event.stopPropagation();
};

qm.input.onkeyup = (event) => {
    event.stopPropagation();
    if (qm.input.value.length > 2) {
        qm.searchIcon.classList.remove('hidden');
    } else {
        qm.searchIcon.classList.add('hidden');
    }
    if (event.keyCode === 13) {
        qm.userInput();
    }
};

qm.searchIcon.onclick = (event) => {
    event.stopPropagation();
    qm.userInput();
};

/* qm.input.onblur = (event) => {
    event.stopPropagation();
    qm.userInput();
}; */

qm.input.onfocus = (event) => {
    event.stopPropagation();
    qm.searchIcon.classList.add('hidden');
    qm.input.value = '';
};

window.onresize = () => {
    qm.setQuoteArea();
    qm.setFontSize();
    qm.adjustFontSize();
    qm.mode === 'random' && qm.adjustAttribution();
    qm.modeSelect.style.fontSize = qm.quoteArea * 1.3 + 'em';
};

window.onbeforeunload = () => {
    if (localStorage && qm.userTitles.length) {
        localStorage.userTitles = JSON.stringify(qm.userTitles);
    }
};

window.onload = () => {
    // Add user’s previously entered titles to default titles array
    if (localStorage && localStorage.userTitles) {
        qm.userTitles = JSON.parse(localStorage.userTitles);
        window.titles = window.titles || [];
        window.titles = window.titles.concat(qm.userTitles);
    }
    // Get page font size in case user has text zoom enabled
    qm.pageFontSize = Number(getComputedStyle(qm.machine, '').fontSize.match(/(\d*(\.\d*)?)px/)[1]);
    qm.setQuoteArea();
    qm.modeSelect.style.fontSize = qm.quoteArea * 1.3 + 'em';

    // If random mode then check jQuery and wqa are loaded
    if (qm.mode === 'random' && (typeof $ !== 'function' || typeof wqa !== 'object')) {
        qm.showQuote('<span class="error">Failed to load properly.</span>');
    } else {
        let pathname = window.location.pathname;
        if (pathname && pathname !== '/') {
            qm.userInput(pathname.replace(/\//g, '').replace(/_/g, ' '));
        } else {
            qm.getQuote();
            qm.changeQuote();
        }
    }
    qm.hasFeatures() || alert('Your browser does not support modern standards. Quote Machine cannot display properly.');
};

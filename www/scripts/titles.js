var titles = [

    'Anonymous',

    // Ancient
    'Aristotle',
    'Buddha',
    'Jesus',
    'Julius Caesar',
    'Marcus Aurelius',
    'Muhammad',
    'Rumi',
    'Socrates',

    //Activists
    'Eleanor Roosevelt',
    'Timothy Leary',

    // Artists
    'Banksy',
    'Yoko Ono',
    
    // Comedians
    'Bill Burr',
    'Bill Cosby',
    'Bill Hicks',
    'Bill Maher',
    'Dylan Moran',
    'Eddie Murphy',
    'George Carlin',
    'Jerry Seinfeld',
    'John Cleese',
    'Jon Stewart',
    'Lewis Black',
    'Peter Sellers',
    'Robin Williams',
    'Russell Brand',
    'Sarah Silverman',
    'Stephen Fry',
    'Tim Allen',

    //Musicians
    'John Lennon',
    'Ludwig van Beethoven',

    // Politicians
    'Abraham Lincoln',
    'Adolf Hitler',
    'Barack Obama',
    'Bill Clinton',
    'Bernie Sanders',
    'Donald Trump',
    'Franklin D. Roosevelt',
    'George Washington',
    'Hillary Clinton',
    'John F. Kennedy',
    'Joseph Stalin',
    'Mahatma Gandhi',
    'Margaret Thatcher',
    'Saddam Hussein',
    'Thomas Jefferson',
    'Winston Churchill',

    // Scientists
    'Albert Einstein',
    'Bill Nye',
    'Brian Greene',
    'Charles Darwin',
    'Galileo Galilei',
    'Isaac Newton',
    'Neil deGrasse Tyson',
    'Richard Dawkins',
    'Richard Feynman',
    'Stephen Hawking',

    // Skeptics
    'Carl Sagan',
    'Christopher Hitchens',
    'Daniel Dennett',
    'David Hume',
    'Friedrich Nietzsche',
    'Sam Harris',

    // Writers
    'Bertrand Russell',
    'Charles Dickens',
    'Douglas Adams',
    'George Bernard Shaw',
    'Helen Keller',
    'Isaac Asimov',
    'L. Ron Hubbard',
    'Leo Tolstoy',
    'Scott Adams',
    'Starhawk',
    'Ralph Waldo Emerson',
    'T. S. Eliot',
    'Thomas Paine',
    'William Shakespeare',

    // Books
    'Alice\'s Adventures in Wonderland',
    'Animal Farm',
    'Life of Pi',
    'Nineteen Eighty-Four',
    'Principia Discordia',
    'The Hitchhiker\'s Guide to the Galaxy',
    'The Prophet',

    // Movies
    'American Psycho',
    'Apocalypse Now',
    'Back to the Future',
    'Forrest Gump',
    'Life of Brian',
    'The Dark Knight',
    'The Godfather',
    'The Shawshank Redemption',
    'Wayne\'s World'

];

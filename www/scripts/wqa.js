/**
 * Adapted from the script at https://github.com/natetyler/wikiquotes-api
 */
var wqa = {
    url: 'https://en.wikiquote.org/w/api.php',

    /**
     * Perform query based on 'titles' parameter and return page id.
     * If multiple page ids are returned, choose the first one.
     * Query includes 'redirects' option to automatically traverse redirects.
     * All words will be capitalized as this generally yields better results.
     */
    queryTitles(titles, success, error) {
        $.ajax({
            url: wqa.url,
            dataType: 'jsonp',
            data: {
                format: 'json',
                action: 'query',
                redirects: '',
                titles: titles
            },
            success: (result, status) => {
                var pages = result.query.pages,
                    pageId = -1;
                
                for (let index in pages) {
                    let page = pages[index];

                    // API can return invalid records - these are marked as 'missing'
                    if (!('missing' in page)) {
                        pageId = page.pageid;
                        break;
                    }
                }
                if (pageId > 0) {
                    success(pageId);
                } else {
                    error('No results');
                }
            },
            error: (xhr, result, status) => {
                error('Error processing your query');
            }
        });
    },


    /**
     * Get the sections for a given page. This makes parsing for quotes more manageable.
     * Returns an array of all '1.x' sections as these usually contain the quotes.
     * If no 1.x sections exist, returns section 1. Returns the titles that were used
     * in case there is a redirect.
     */
    getSectionsForPage(pageId, success, error) {
        $.ajax({
            url: wqa.url,
            dataType: 'jsonp',
            data: {
                format: 'json',
                action: 'parse',
                prop: 'sections',
                pageid: pageId
            },
            success: (result, status) => {
                var sectionArray = [],
                    sections = result.parse.sections;
    
                for (let section in sections) {
                    let splitNum = sections[section].number.split('.');
                    if (splitNum.length > 1 && splitNum[0] === '1') {
                        sectionArray.push(sections[section].index);
                    }
                }
                // Use section 1 if there are no '1.x' sections
                if(sectionArray.length === 0) {
                    sectionArray.push('1');
                }
                success({
                    titles: result.parse.title,
                    sections: sectionArray
                });
            },
            error: (xhr, result, status) => {
                error('Error getting sections');
            }
        });
    },


    /**
     * Get all quotes for a given section.  Most sections will be of the format:
     * <h3> title </h3>
     * <ul>
     *   <li> 
     *     Quote text
     *     <ul>
     *       <li> additional info on the quote </li>
     *     </ul>
     *   </li>
     * <ul>
     * <ul> next quote etc... </ul>
     *
     * The quote may or may not contain sections inside <b /> tags.
     *
     * For quotes with bold sections, only the bold part is returned for brevity
     * (usually the bold part is more well known).
     * Otherwise the entire text is returned.  Returns the titles that were used
     * in case there is a redirect.
     */
    getQuotesForSection(pageId, sectionIndex, success, error) {
        $.ajax({
            url: wqa.url,
            dataType: 'jsonp',
            data: {
                format: 'json',
                action: 'parse',
                noimages: '',
                pageid: pageId,
                section: sectionIndex
            },
            success: (result, status) => {
                var quotes = result.parse.text['*'],
                    quoteArray = [],

                    // Find top level <li> only
                    $listItems = $(quotes).find('li:not(li li)');

                $listItems.each(function () {
                    var $bolds;
                    // Remove all children that aren't <b>
                    //$(this).children().remove('ul, ol, li');
                    $(this).children().remove(':not(b, a)');
                    $bolds = $(this).find('b');

                    // If the section has bold text then use it,
                    // otherwise pull the plain text
                    if ($bolds.length > 0) {
                        quoteArray.push($bolds.html());
                    } else {
                        quoteArray.push($(this).html());
                    }
                });
                success({
                    titles: result.parse.title,
                    quotes: quoteArray
                });
            },
            error: (xhr, result, status) => {
                error('Error getting quotes');
            }
        });
    },


    /**
     * Search using opensearch API. Returns an array of search results.
     */
    openSearch(titles, success, error) {
        $.ajax({
            url: wqa.url,
            dataType: 'jsonp',
            data: {
                format: 'json',
                action: 'opensearch',
                namespace: 0,
                suggest: '',
                search: titles
            },
            success: (result, status) => {
                success(result[1]);
            },
            error: (xhr, result, status) => {
                error('Error with opensearch for ' + titles);
            }
        });
    },


    /**
     * Get a random quote for the given title search.
     * This function searches for a page ID for the given title, chooses a random
     * section from the list of sections for the page, then chooses a random quote
     * from that section. Returns the titles used in case there is a redirect.
     */
    getRandomQuote(titles, success, error) {
        function errorFunc(msg) {
            error(msg);
        }

        function chooseQuote(quotes) {
            var randomNum = Math.floor(Math.random() * quotes.quotes.length);
            success({
                titles: quotes.titles,
                quote: quotes.quotes[randomNum]
            });
        }

        function getQuotes(pageId, sections) {
            var randomNum = Math.floor(Math.random() * sections.sections.length);
            wqa.getQuotesForSection(pageId, sections.sections[randomNum], chooseQuote, errorFunc);
        }

        function getSections(pageId) {
            wqa.getSectionsForPage(pageId, (sections) => {
                getQuotes(pageId, sections);
            }, errorFunc);
        }

        wqa.queryTitles(titles, getSections, errorFunc);
    },


    /**
     * Gets multiple random quotes for the given title search.
     * This function searches for a page ID for the given title,
     * then gets the quotes from a random section on the page.
     */
    getRandomQuotes(titles, success, error) {
        function errorFunc(msg) {
            error(msg);
        }

        function getQuotes(pageId, sections) {
            var randomNum = Math.floor(Math.random() * sections.sections.length);
            wqa.getQuotesForSection(pageId, sections.sections[randomNum], success, errorFunc);
        }

        function getSections(pageId) {
            wqa.getSectionsForPage(pageId, (sections) => {
                getQuotes(pageId, sections);
            }, errorFunc);
        }

        wqa.queryTitles(titles, getSections, errorFunc);
    },



    /**
     * Capitalize the first letter of each word.
     */
    capitalizeString(input) {
        var inputArray = input.split(' '),
            output = [];
        
        for (let word in inputArray) {
            output.push(inputArray[word].charAt(0).toUpperCase() + inputArray[word].slice(1));
        }
        return output.join(' ');
    }
};
